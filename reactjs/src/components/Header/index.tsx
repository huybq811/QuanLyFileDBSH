import './index.less';

import * as React from 'react';

import { Avatar, Col, Dropdown, Menu, Row } from 'antd';
import { MenuUnfoldOutlined, MenuFoldOutlined, LogoutOutlined } from '@ant-design/icons';

import { L } from '../../lib/abpUtility';
import { Link } from 'react-router-dom';

import profilePicture from '../../images/user.png';

export interface IHeaderProps {
  collapsed?: any;
  toggle?: any;
}

const userDropdownMenu = (
  <Menu>
    <Menu.Item key="2">
      <Link to="/logout">
        <LogoutOutlined />
        <span> {L('Logout')}</span>
      </Link>
    </Menu.Item>
  </Menu>
);

export class Header extends React.Component<IHeaderProps> {
  render() {
    return (
      <Row className={'header-container'}>
        <Col style={{ textAlign: 'left' }} span={20}>
          <div className='left-content'>
            {this.props.collapsed ? (
              <MenuUnfoldOutlined className="trigger" onClick={this.props.toggle} />
            ) : (
              <MenuFoldOutlined className="trigger" onClick={this.props.toggle} />
            )}
            <div className='title'>HTTT KIỂM SOÁT AN TOÀN ĐẬP THÀNH PHỐ HÀ NỘI</div>
          </div>
        </Col>
        <Col style={{ padding: '0px 15px 0px 15px', textAlign: 'right' }} span={4}>
          <Dropdown overlay={userDropdownMenu} trigger={['click']}>
            <Avatar style={{ height: 24, width: 24 }} shape="circle" alt={'profile'} src={profilePicture} />
          </Dropdown>
        </Col>
      </Row>
    );
  }
}

export default Header;
