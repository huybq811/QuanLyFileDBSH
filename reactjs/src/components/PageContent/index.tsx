import { Breadcrumb, Col, Row } from 'antd';
import React, { useCallback } from 'react'
import './index.less';

interface PageContentProps {
    children?: any;
    breadcrumb?: Array<{ title: string, onClick?: any, href?: any }>;
    title?: string;
    extra?: any;
    className?: string | undefined;
    emptyHeader?: boolean
    extraContent?: any
}

function PageContent(props: PageContentProps) {
    const { breadcrumb, children, extra, title, className, emptyHeader, extraContent } = props

    const BreadCrumbRender = () => {
        if (!breadcrumb) return null;
        return (
            <Breadcrumb>
                {breadcrumb?.map(crumb => (<Breadcrumb.Item href={crumb.href} key={crumb.title} onClick={crumb.onClick ? crumb.onClick : undefined} >{crumb.title}</Breadcrumb.Item>))}
            </Breadcrumb>
        )
    }
    const Header = useCallback(
        () => {
            if (emptyHeader) return null;
            return (
                <div className='page-header'>
                    <BreadCrumbRender />
                    <Row justify='space-between'>
                        <Col span={8}>
                            {/* <div className="text-h4-medium">{title}</div> */}
                        </Col>
                        <Col span={16}>
                            {extra}
                        </Col>
                    </Row>
                    {extraContent}
                </div>
            )
        },
        [title, emptyHeader, extraContent, extra],
    )

    return (
        <div className={`page-container-wrapper ${className}`}>
            <Header />
            <div className='page-content'>
                {children}
            </div>
        </div>
    )
}

export default PageContent