export const URL_PATH = {
    Home: '/',
    BAI_VIET: '/bai-viet',
    HE_THONG: '/he-thong',
    THANH_PHAN_HE_THONG: '/thanh-phan-he-thong',
    CHUC_NANG: '/chuc-nang',
    TAI_KHOAN: '/users',
    TAO_BAI_VIET: '/crud-bai-viet',
}