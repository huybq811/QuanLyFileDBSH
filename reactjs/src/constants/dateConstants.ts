export const DateFormat = {
    DDMMYY: 'DD/MM/YYYY',
    YYYYMMDD: 'YYYY-MM-DD'
}