import { DUONG_GIOI_HAN } from "./enums";

export const DuongGioHanOptions = [
    { value: DUONG_GIOI_HAN.PHAI, label: "Giới hạn phải" },
    { value: DUONG_GIOI_HAN.TRAI, label: "Giới hạn trái" }
]

export const HAM_LON_CODE = "HHL"