import { Col, Form, Input, Row } from 'antd';
import React from 'react'
import FormModal, { IFormModalProps } from '../../../components/Modal/FormModal';
import rules from '../../../constants/index.validation';

export interface ICreateOrUpdateUserProps extends IFormModalProps {
}

function CreateOrUpdateSystemModal(props: ICreateOrUpdateUserProps) {
    return (
        <FormModal {...props}>
            <Row gutter={[12, 12]}>
                <Col span={24}>
                    <Form.Item label={"Tên hệ thống"} name={'name'} rules={rules.required}>
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label={"Mô tả"} rules={rules.required} name={'description'}>
                        <Input.TextArea />
                    </Form.Item>
                </Col>

                <Form.Item hidden name={'id'} />
            </Row>
        </FormModal>

    )
}

export default CreateOrUpdateSystemModal