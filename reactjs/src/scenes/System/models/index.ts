import SystemStore from "../../../stores/systemStore";

export interface ISystemProps {
  systemStore: SystemStore
}

export interface ISystemState {
  modalVisible: boolean;
  maxResultCount: number;
  skipCount: number;
  userId: number;
  filter: string;
  loading: boolean;
}