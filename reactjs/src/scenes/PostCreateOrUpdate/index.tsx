import { Card, Col, Row } from 'antd'
import React, { Component } from 'react'
import PageContent from '../../components/PageContent'
import { URL_PATH } from '../../components/Router/router.path'

export default class PostCreateOrUpdatePage extends Component {
    render() {
        return (
            <PageContent
                breadcrumb={[
                    { title: "Trang chủ", href: URL_PATH.Home },
                    { title: "Bài viết", href: URL_PATH.BAI_VIET },
                    { title: "Tạo bài viết" },
                ]}>

                <Card size='small' className='px-12'>
                    <Row justify='space-between' align='middle' gutter={12}>
                        <Col>
                            <h2 className='mb-0'>Tạo bài viết</h2>
                        </Col>
                    </Row>
                </Card>

            </PageContent>
        )
    }
}
