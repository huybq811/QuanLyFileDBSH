import { RouteComponentProps } from "react-router-dom";
import SystemStore from "../../../stores/systemStore";

export interface IPostProps extends RouteComponentProps {
    systemStore: SystemStore

}

export interface IPostState {
    modalVisible: boolean;
    maxResultCount: number;
    skipCount: number;
    filter: string;
    loading: boolean;
    objectSystemId: any;
    systemDataId: any;
    selectedEdit: any;
    modalVisibleCreate: boolean,
}