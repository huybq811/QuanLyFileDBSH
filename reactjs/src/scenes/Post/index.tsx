import React from 'react'
import AppComponentBase from '../../components/AppComponentBase'
import { IPostProps, IPostState } from './models'
import { inject, observer } from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import PageContent from '../../components/PageContent';
import { URL_PATH } from '../../components/Router/router.path';
import { Button, Card, Col, Dropdown, Input, Menu, Modal, Row, Table } from 'antd';
import { FilterOutlined, PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { FormInstance } from 'rc-field-form';
import FilterPostModal from './components/filterPostModal';
import { ColumnsType } from 'antd/lib/table';
import THeader from '../../components/Tables/tableHeader';
import CreateOrUpdatePostModal from './components/createOrUpdatePostModal';
const Search = Input.Search;
const confirm = Modal.confirm;

@inject(Stores.SystemStore, Stores.SessionStore)
@observer
class PostPage extends AppComponentBase<IPostProps, IPostState> {

    formRef = React.createRef<FormInstance>();

    state = {
        modalVisible: false,
        modalVisibleCreate: false,
        maxResultCount: 10,
        skipCount: 0,
        filter: '',
        loading: false,
        systemDataId: undefined,
        objectSystemId: undefined,
        selectedEdit: {},
    };

    onOpenFilter = () => {
        this.setState({ modalVisible: true })
    }

    handleFilter = () => {
        const form = this.formRef.current;
        form!.validateFields().then(async (values: any) => {
            this.setState({ loading: true })
            await this.props.systemStore.createOreUpdateFeature(values);
            this.setState({ modalVisible: false });
            // await this.getAll();
            form!.resetFields();
            this.setState({ loading: false })
        });
    };

    async getAll() {
        this.setState({ loading: true })
        await this.props.systemStore.getAllPost({
            maxResultCount: this.state.maxResultCount,
            skipCount: this.state.skipCount,
            filter: this.state.filter,
            systemDataId: this.state.systemDataId,
            objectSystemId: this.state.objectSystemId,
            objectCategoryId: undefined
        });
        this.setState({ loading: false })
    }

    async componentDidMount() {
        await this.getAll();
    }
    handleTableChange = (pagination: any) => {
        this.setState({ skipCount: (pagination.current - 1) * this.state.maxResultCount! }, async () => await this.getAll());
    };
    Modal = () => {
        this.setState({
            modalVisible: !this.state.modalVisible,
        });
    };
    ModalCreate = () => {
        this.setState({
            modalVisibleCreate: !this.state.modalVisibleCreate,
        });
    };
    createOrUpdateModalOpen = (entity?: any) => {
        this.props.history.push(URL_PATH.TAO_BAI_VIET)
        // this.ModalCreate();
        // if (entity) {
        //     this.setState({ selectedEdit: entity })
        //     setTimeout(() => {
        //         this.formRef.current?.setFieldsValue({ ...entity });
        //     }, 100);
        // }
    }
    delete(id: any) {
        const self = this;
        confirm({
            title: 'Bạn muốn xóa bản ghi này?',
            onOk() {
                self.props.systemStore.deleteFeature({ id }).finally(() => {
                    self.getAll()
                });
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    handleCreate = () => {
        // const form = this.formRef.current;
        // form!.validateFields().then(async (values: any) => {
        //     this.setState({ loading: true })
        //     await this.props.systemStore.createOreUpdateFeature(values);
        //     this.setState({ modalVisible: false });
        //     await this.getAll();
        //     form!.resetFields();
        //     this.setState({ loading: false })
        // });
    };

    render() {
        const { postList } = this.props.systemStore;

        const columns: ColumnsType<any> = [
            { title: <THeader>Tiêu đề</THeader>, dataIndex: 'title', key: 'title' },
            { title: <THeader>Nội dung</THeader>, dataIndex: 'content', key: 'content' },
            { title: <THeader>Mô tả</THeader>, dataIndex: 'description', key: 'description' },
            {
                title: <THeader>Hành động</THeader>,
                dataIndex: 'action',
                width: 120,
                key: 'action',
                render: (_, record) => {
                    return (
                        <Dropdown
                            overlay={
                                <Menu>
                                    <Menu.Item
                                        onClick={() => { this.createOrUpdateModalOpen(record) }}>Chỉnh sửa chức năng</Menu.Item>
                                    <Menu.Item
                                        onClick={() => { this.delete(record.id) }}>Xóa chức năng</Menu.Item>
                                </Menu>
                            } trigger={['click']}>
                            <Button icon={<SettingOutlined />} type='primary' />
                        </Dropdown>
                    )
                }
            },
        ];

        return (
            <PageContent
                breadcrumb={[{ title: "Trang chủ", href: URL_PATH.Home }, { title: "Bài viết" }]}>

                <Card size='small' className='px-12'>
                    <Row justify='space-between' align='middle' gutter={12}>
                        <Col>
                            <h2 className='mb-0'>Danh sách bài viết</h2>
                        </Col>
                        <Row gutter={12}>
                            <Col>
                                <Search placeholder={"Tìm kiếm"}
                                // onSearch={this.handleSearch} 
                                />
                            </Col>

                            <Col >
                                <Button
                                    type='primary' icon={<FilterOutlined />}
                                    onClick={this.onOpenFilter}
                                >Bộ lọc</Button>
                            </Col>
                            <Col >
                                <Button
                                    type='primary' icon={<PlusOutlined />}
                                    onClick={this.createOrUpdateModalOpen}
                                >Tạo mới</Button>
                            </Col>
                        </Row>
                    </Row>
                </Card>

                <Card className='mt-12'>
                    <Table
                        rowKey={(record) => record.id.toString()}
                        bordered={true}
                        columns={columns}
                        pagination={{ pageSize: 10, total: postList === undefined ? 0 : postList.totalCount, defaultCurrent: 1 }}
                        loading={this.state.loading}
                        dataSource={postList === undefined ? [] : postList.items}
                        onChange={this.handleTableChange}
                        scroll={{ x: 500 }}
                    />
                </Card>


                <FilterPostModal
                    title='Bộ lọc'
                    formRef={this.formRef}
                    visible={this.state.modalVisible}
                    confirmLoading={this.state.loading}
                    onCancel={() => {
                        this.setState({
                            modalVisible: false,
                        });
                        this.formRef.current?.resetFields();
                    }}
                    onCreate={this.handleFilter}
                />

                <CreateOrUpdatePostModal
                    title='Bài viết'
                    formRef={this.formRef}
                    visible={this.state.modalVisibleCreate}
                    confirmLoading={this.state.loading}
                    onCancel={() => {
                        this.setState({
                            modalVisibleCreate: false,
                        });
                        this.formRef.current?.resetFields();
                    }}
                    onCreate={this.handleCreate}
                    entity={this.state.selectedEdit}
                />


            </PageContent>
        )
    }
}


export default PostPage
