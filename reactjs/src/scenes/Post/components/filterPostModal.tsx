import { Row } from 'antd'
import React, { useState } from 'react'
import FormModal, { IFormModalProps } from '../../../components/Modal/FormModal'
import SelectItem from '../../../components/Select/SelectItem'
import systemService from '../../../services/system/systemService'


export interface ICreateOrUpdatePostProps extends IFormModalProps {
}

export default function FilterPostModal(props: ICreateOrUpdatePostProps) {
    const [system, setSystem] = useState()
    const [systemCmp, setSystemCmp] = useState()

    const onChangeSystem = (value: any) => {
        setSystem(value)
        props.formRef?.current?.resetFields(['objectSystemId']);
    }
    const onChangeSystemCmp = (value: any) => {
        setSystemCmp(value)
        props.formRef?.current?.resetFields(['objectSystemId']);
    }


    return (
        <FormModal {...props}>
            <Row gutter={[12, 12]}>
                <SelectItem
                    formItemProps={{ label: "Hệ thống", name: "systemDataId" }}
                    remoteSource={systemService.getSystemAsOptions}
                    refresing={true}
                    span={24}
                    selectProps={{
                        onChange: onChangeSystem,
                    }}
                />
                <SelectItem
                    formItemProps={{ label: "Thành phần hệ thống", name: "objectSystemId" }}
                    remoteSource={systemService.getSystemCmpAsOptions.bind(null, { heThongId: system })}
                    refresing={system}
                    span={24}
                    selectProps={{
                        onChange: onChangeSystemCmp,
                    }}
                />
                <SelectItem
                    formItemProps={{ label: "Chức năng", name: "objectCategoryId" }}
                    remoteSource={systemService.getFeatureAsOptions.bind(null, { tpHeThongId: systemCmp })}
                    refresing={systemCmp}
                    span={24}
                />
            </Row>
        </FormModal>
    )
}
