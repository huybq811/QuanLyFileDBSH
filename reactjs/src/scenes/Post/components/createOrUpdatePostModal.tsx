import { UploadOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Row, Upload } from 'antd';
import React, { useCallback, useEffect, useState } from 'react'
import FormModal, { IFormModalProps } from '../../../components/Modal/FormModal';
import SelectItem from '../../../components/Select/SelectItem';
import rules from '../../../constants/index.validation';
import systemService from '../../../services/system/systemService';

export interface ICreateOrUpdatePostProps extends IFormModalProps {
    entity: any
}

function CreateOrUpdatePostModal(props: ICreateOrUpdatePostProps) {
    const { entity } = props
    const [system, setSystem] = useState(entity?.systemDataId)
    const [systemCmp, setSystemCmp] = useState(entity?.objectSystemId)

    const onChangeSystem = (value: any) => {
        setSystem(value)
        props.formRef?.current?.resetFields(['objectSystemId']);
    }
    const onChangeSystemCmp = (value: any) => {
        setSystemCmp(value)
        props.formRef?.current?.resetFields(['objectCategoryId']);
    }

    const genSystem = useCallback(
        () => {
            return (
                <SelectItem
                    formItemProps={{ label: "Hệ thống", name: "systemDataId", rules: rules.required }}
                    remoteSource={systemService.getSystemAsOptions}
                    refresing={true}
                    span={12}
                    selectProps={{
                        onChange: onChangeSystem,
                    }}
                />
            )
        },
        [],
    )
    const genSysCmp = useCallback(
        () => {
            return (
                <SelectItem
                    formItemProps={{ label: "Thành phần hệ thống", name: "objectSystemId", rules: rules.required }}
                    remoteSource={systemService.getSystemCmpAsOptions.bind(null, { heThongId: system })}
                    refresing={system}
                    span={12}
                    selectProps={{
                        onChange: onChangeSystemCmp,
                    }}
                />
            )
        },
        [system],
    )
    const genFeature = useCallback(
        () => {
            return (
                <SelectItem
                    formItemProps={{ label: "Chức năng", name: "objectCategoryId", rules: rules.required }}
                    remoteSource={systemService.getFeatureAsOptions.bind(null, { tpHeThongId: systemCmp })}
                    refresing={systemCmp}
                    span={12}
                />
            )
        },
        [systemCmp],
    )

    useEffect(() => {
        setSystem(entity.systemDataId)
    }, [entity.systemDataId])

    return (
        <FormModal {...props}>
            <Row gutter={[12, 12]}>
                {genSystem()}
                {genSysCmp()}
                {genFeature()}

                <Col span={12}>
                    <Form.Item label="Tên chức năng" name="name" rules={rules.required}>
                        <Input placeholder='Nhập' />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label="Mô tả" name="description">
                        <Input placeholder='Nhập' />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Upload
                        listType="picture"
                        multiple
                        beforeUpload={() => {
                            return false;
                        }}
                        onChange={(info) => {
                            console.log("INFO", info)
                        }}

                    >
                        <Button icon={<UploadOutlined />}>Upload (Max: 3)</Button>
                    </Upload>
                </Col>

            </Row>
            <Form.Item hidden name={'id'} />
        </FormModal>

    )
}

export default CreateOrUpdatePostModal