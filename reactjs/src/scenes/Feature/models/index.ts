import SystemStore from "../../../stores/systemStore";

export interface IFeatureProps {
    systemStore: SystemStore
}

export interface IFeatureState {
  modalVisible: boolean;
  maxResultCount: number;
  skipCount: number;
  filter: string;
  loading: boolean;
  objectSystemId: any;
  systemDataId: any;
  selectedEdit: any;
}