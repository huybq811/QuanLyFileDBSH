import { Col, Form, Input, Row } from 'antd';
import React, { useCallback, useEffect, useState } from 'react'
import FormModal, { IFormModalProps } from '../../../components/Modal/FormModal';
import SelectItem from '../../../components/Select/SelectItem';
import rules from '../../../constants/index.validation';
import systemService from '../../../services/system/systemService';

export interface ICreateOrUpdateFeatureProps extends IFormModalProps {
    entity: any
}

function CreateOrUpdateFeatureModal(props: ICreateOrUpdateFeatureProps) {
    const { entity } = props
    const [system, setSystem] = useState(entity?.systemDataId)

    const onChangeSystem = (value: any) => {
        setSystem(value)
        props.formRef?.current?.resetFields(['objectSystemId']);
    }

    const genSystem = useCallback(
        () => {
            return (
                <SelectItem
                    formItemProps={{ label: "Hệ thống", name: "systemDataId", rules: rules.required }}
                    remoteSource={systemService.getSystemAsOptions}
                    refresing={true}
                    span={24}
                    selectProps={{
                        onChange: onChangeSystem,
                    }}
                />
            )
        },
        [],
    )
    const genSysCmp = useCallback(
        () => {
            return (
                <SelectItem
                    formItemProps={{ label: "Thành phần hệ thống", name: "objectSystemId", rules: rules.required }}
                    remoteSource={systemService.getSystemCmpAsOptions.bind(null, { heThongId: system })}
                    refresing={system}
                    span={24}
                />
            )
        },
        [system],
    )

    useEffect(() => {
        setSystem(entity.systemDataId)
    }, [entity.systemDataId])

    return (
        <FormModal {...props}>
            <Row gutter={[12, 12]}>
                {genSystem()}
                {genSysCmp()}

                <Col span={24}>
                    <Form.Item label="Tên chức năng" name="name" rules={rules.required}>
                        <Input placeholder='Nhập' />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label="Mô tả" name="description">
                        <Input placeholder='Nhập' />
                    </Form.Item>
                </Col>

            </Row>
            <Form.Item hidden name={'id'} />
        </FormModal>

    )
}

export default CreateOrUpdateFeatureModal