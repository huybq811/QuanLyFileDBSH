import React from 'react'
import { inject, observer } from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import AppComponentBase from '../../components/AppComponentBase';
import { IFeatureProps, IFeatureState } from './models';
import PageContent from '../../components/PageContent';
import { URL_PATH } from '../../components/Router/router.path';
import { Button, Card, Col, Dropdown, Input, Menu, Modal, Row, Table } from 'antd';
import SelectItem from '../../components/Select/SelectItem';
import systemService from '../../services/system/systemService';
import { PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { ColumnsType } from 'antd/lib/table';
import THeader from '../../components/Tables/tableHeader';
import { FormInstance } from 'antd/lib/form';
import CreateOrUpdateFeatureModal from './components/createOrUpdateFeatureModal';
const Search = Input.Search;
const confirm = Modal.confirm;

@inject(Stores.SystemStore, Stores.SessionStore)
@observer
class FeaturePage extends AppComponentBase<IFeatureProps, IFeatureState> {
  formRef = React.createRef<FormInstance>();

  state = {
    modalVisible: false,
    maxResultCount: 10,
    skipCount: 0,
    filter: '',
    loading: false,
    systemDataId: undefined,
    objectSystemId: undefined,
    selectedEdit: {}
  };

  handleSysFilter = (value: string) => {
    this.setState({ systemDataId: value, objectSystemId: undefined },
      async () => await this.getAll()
    );
  }
  handleSysCmpFilter = (value: string) => {
    this.setState({ objectSystemId: value },
      async () => await this.getAll()
    );
  }
  async getAll() {
    this.setState({ loading: true })
    await this.props.systemStore.getAllFeature({
      maxResultCount: this.state.maxResultCount,
      skipCount: this.state.skipCount,
      filter: this.state.filter,
      systemDataId: this.state.systemDataId,
      objectSystemId: this.state.objectSystemId
    });
    this.setState({ loading: false })
  }

  async componentDidMount() {
    await this.getAll();
  }
  handleTableChange = (pagination: any) => {
    this.setState({ skipCount: (pagination.current - 1) * this.state.maxResultCount! }, async () => await this.getAll());
  };
  Modal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  };
  createOrUpdateModalOpen = (entity?: any) => {
    this.Modal();
    if (entity) {
      this.setState({ selectedEdit: entity })
      setTimeout(() => {
        this.formRef.current?.setFieldsValue({ ...entity });
      }, 100);
    }
  }
  delete(id: any) {
    const self = this;
    confirm({
      title: 'Bạn muốn xóa bản ghi này?',
      onOk() {
        self.props.systemStore.deleteFeature({ id }).finally(() => {
          self.getAll()
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  handleSearch = (value: string) => {
    this.setState({ filter: value }, async () => await this.getAll());
  };
  handleCreate = () => {
    const form = this.formRef.current;
    form!.validateFields().then(async (values: any) => {
      this.setState({ loading: true })
      await this.props.systemStore.createOreUpdateFeature(values);
      this.setState({ modalVisible: false });
      await this.getAll();
      form!.resetFields();
      this.setState({ loading: false })
    });
  };

  render() {
    const { featureList } = this.props.systemStore;

    const columns: ColumnsType<any> = [
      { title: <THeader>Tên chức năng</THeader>, dataIndex: 'name', key: 'name' },
      { title: <THeader>Tên hệ thống</THeader>, dataIndex: 'systemDataName', key: 'systemDataName' },
      { title: <THeader>Tên thành phần hệ thống</THeader>, dataIndex: 'objectSystemName', key: 'objectSystemName' },
      { title: <THeader>Mô tả</THeader>, dataIndex: 'description', key: 'description' },
      {
        title: <THeader>Hành động</THeader>,
        dataIndex: 'action',
        width: 120,
        key: 'action',
        render: (_, record) => {
          return (
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item
                    onClick={() => { this.createOrUpdateModalOpen(record) }}>Chỉnh sửa chức năng</Menu.Item>
                  <Menu.Item
                    onClick={() => { this.delete(record.id) }}>Xóa chức năng</Menu.Item>
                </Menu>
              } trigger={['click']}>
              <Button icon={<SettingOutlined />} type='primary' />
            </Dropdown>
          )
        }
      },
    ];

    return (
      <PageContent
        breadcrumb={[
          { title: "Trang chủ", href: URL_PATH.Home },
          { title: "Chức năng" },
        ]}>
        <Card size='small' className='px-12'>
          <Row justify='space-between' align='middle' gutter={12}>
            <Col>
              <h2 className='mb-0'>Danh sách chức năng</h2>
            </Col>
            <Row gutter={12}>
              <Col>
                <SelectItem
                  span={24}
                  remoteSource={systemService.getSystemAsOptions}
                  refresing={true}
                  className='mb-0'
                  selectProps={{
                    showSearch: true,
                    allowClear: true,
                    filterOption: (input: any, option: any) => option?.label?.toLowerCase().includes(input.toLowerCase()),
                    onChange: this.handleSysFilter,
                    placeholder: "Chọn hệ thống",
                    style: { minWidth: 200 }

                  }}
                />
              </Col>
              <Col>
                <SelectItem
                  span={24}
                  remoteSource={systemService.getSystemCmpAsOptions.bind(null, { heThongId: this.state.systemDataId })}
                  refresing={this.state.systemDataId}
                  className='mb-0'
                  selectProps={{
                    showSearch: true,
                    allowClear: true,
                    filterOption: (input: any, option: any) => option?.label?.toLowerCase().includes(input.toLowerCase()),
                    onChange: this.handleSysCmpFilter,
                    placeholder: "Chọn thành phần hệ thống",
                    style: { minWidth: 200 },
                    value: this.state.objectSystemId
                  }}
                />
              </Col>
              <Col>
                <Search placeholder={"Tìm kiếm"}
                  onSearch={this.handleSearch}
                />
              </Col>
              <Col >
                <Button
                  type='primary' icon={<PlusOutlined />}
                  onClick={this.createOrUpdateModalOpen}
                >Tạo mới</Button>
              </Col>
            </Row>
          </Row>
        </Card>

        <Card className='mt-12'>
          <Table
            rowKey={(record) => record.id.toString()}
            bordered={true}
            columns={columns}
            pagination={{ pageSize: 10, total: featureList === undefined ? 0 : featureList.totalCount, defaultCurrent: 1 }}
            loading={this.state.loading}
            dataSource={featureList === undefined ? [] : featureList.items}
            onChange={this.handleTableChange}
            scroll={{ x: 500 }}
          />
        </Card>
        <CreateOrUpdateFeatureModal
          title='Chức năng'
          formRef={this.formRef}
          visible={this.state.modalVisible}
          confirmLoading={this.state.loading}
          onCancel={() => {
            this.setState({
              modalVisible: false,
            });
            this.formRef.current?.resetFields();
          }}
          onCreate={this.handleCreate}
          entity={this.state.selectedEdit}
        />


      </PageContent>
    )
  }
}

export default FeaturePage