import SystemStore from "../../../stores/systemStore";

export interface ISystemCmpProps {
  systemStore: SystemStore
}

export interface ISystemCmpState {
  modalVisible: boolean;
  maxResultCount: number;
  skipCount: number;
  userId: number;
  filter: string;
  loading: boolean;
  systemDataId: any;
}