import React from 'react'
import { inject, observer } from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import AppComponentBase from '../../components/AppComponentBase';
import { ISystemCmpProps, ISystemCmpState } from './models';
import { FormInstance } from 'antd/lib/form';
import PageContent from '../../components/PageContent';
import { URL_PATH } from '../../components/Router/router.path';
import { Button, Card, Col, Dropdown, Input, Menu, Modal, Row, Table } from 'antd';
import { PlusOutlined, SettingOutlined } from '@ant-design/icons';
import SelectItem from '../../components/Select/SelectItem';
import systemService from '../../services/system/systemService';
import { ColumnsType } from 'antd/lib/table';
import THeader from '../../components/Tables/tableHeader';
import CreateOrUpdateSysCmpModal from './components/createOrUpdateSysCmpModal';

const Search = Input.Search;
const confirm = Modal.confirm;

@inject(Stores.SystemStore, Stores.SessionStore)
@observer
class SystemComponentPage extends AppComponentBase<ISystemCmpProps, ISystemCmpState> {


  formRef = React.createRef<FormInstance>();

  state = {
    modalVisible: false,
    maxResultCount: 10,
    skipCount: 0,
    userId: 0,
    filter: '',
    loading: false,
    systemDataId: undefined,
  };


  handleSearch = (value: string) => {
    this.setState({ filter: value }, async () => await this.getAll());
  };
  handleFilter = (value: string) => {
    this.setState({ systemDataId: value }, async () => await this.getAll());
  }

  async getAll() {
    this.setState({ loading: true })
    await this.props.systemStore.getAllSysCmp({
      maxResultCount: this.state.maxResultCount,
      skipCount: this.state.skipCount,
      filter: this.state.filter,
      systemDataId: this.state.systemDataId
    });
    this.setState({ loading: false })
  }

  Modal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  };
  createOrUpdateModalOpen = (entity?: any) => {
    this.Modal();
    if (entity) {
      setTimeout(() => {
        this.formRef.current?.setFieldsValue({ ...entity });
      }, 100);
    }
  }
  delete(id: any) {
    const self = this;
    confirm({
      title: 'Bạn muốn xóa bản ghi này?',
      onOk() {
        self.props.systemStore.deleteSysCmp({ id }).finally(() => {
          self.getAll()
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  handleTableChange = (pagination: any) => {
    this.setState({ skipCount: (pagination.current - 1) * this.state.maxResultCount! }, async () => await this.getAll());
  };

  async componentDidMount() {
    await this.getAll();
  }

  handleCreate = () => {
    const form = this.formRef.current;
    form!.validateFields().then(async (values: any) => {
      this.setState({ loading: true })
      await this.props.systemStore.createOreUpdateSysCmp(values);
      this.setState({ modalVisible: false });
      await this.getAll();
      form!.resetFields();
      this.setState({ loading: false })
    });
  };

  render() {
    const { systemComponentList } = this.props.systemStore;

    const columns: ColumnsType<any> = [
      { title: <THeader>Tên thành phần hệ thống</THeader>, dataIndex: 'name', key: 'name' },
      { title: <THeader>Mã TP hệ thống</THeader>, dataIndex: 'code', key: 'code' },
      { title: <THeader>Mô tả</THeader>, dataIndex: 'description', key: 'description' },
      { title: <THeader>Tên hệ thống</THeader>, dataIndex: 'tenHeThong', key: 'tenHeThong' },
      {
        title: <THeader>Hành động</THeader>,
        dataIndex: 'action',
        width: 120,
        key: 'action',
        render: (_, record) => {
          return (
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item
                    onClick={() => { this.createOrUpdateModalOpen(record) }}>Chỉnh sửa thành phần hệ thống</Menu.Item>
                  <Menu.Item
                    onClick={() => { this.delete(record.id) }}>Xóa thành phần hệ thống</Menu.Item>
                </Menu>
              } trigger={['click']}>
              <Button icon={<SettingOutlined />} type='primary' />
            </Dropdown>
          )
        }
      },
    ];

    return (
      <PageContent
        breadcrumb={[{ title: "Trang chủ", href: URL_PATH.Home }, { title: "Thành hệ thống" }]}>

        <Card size='small' className='px-12'>
          <Row justify='space-between' align='middle' gutter={12}>
            <Col >
              <h2 className='mb-0'>Danh sách thành phần hệ thống</h2>
            </Col>
            <Row gutter={12}>
              <Col>
                <SelectItem
                  span={24}
                  remoteSource={systemService.getSystemAsOptions}
                  refresing={true}
                  className='mb-0'
                  selectProps={{
                    showSearch: true,
                    allowClear: true,
                    filterOption: (input: any, option: any) => option?.label?.toLowerCase().includes(input.toLowerCase()),
                    onChange: this.handleFilter,
                    placeholder: "Chọn hệ thống",
                    style: { minWidth: 200 }

                  }}
                />
              </Col>
              <Col>
                <Search placeholder={"Tìm kiếm"} onSearch={this.handleSearch} />
              </Col>
              <Col >
                <Button
                  type='primary' icon={<PlusOutlined />} onClick={this.createOrUpdateModalOpen}>Tạo mới</Button>
              </Col>
            </Row>
          </Row>
        </Card>

        <Card className='mt-12'>
          <Table
            rowKey={(record) => record.id.toString()}
            bordered={true}
            columns={columns}
            pagination={{ pageSize: 10, total: systemComponentList === undefined ? 0 : systemComponentList.totalCount, defaultCurrent: 1 }}
            loading={this.state.loading}
            dataSource={systemComponentList === undefined ? [] : systemComponentList.items}
            onChange={this.handleTableChange}
            scroll={{ x: 500 }}
          />
        </Card>
        <CreateOrUpdateSysCmpModal
          title='Thành phần hệ thống'
          formRef={this.formRef}
          visible={this.state.modalVisible}
          confirmLoading={this.state.loading}
          onCancel={() => {
            this.setState({
              modalVisible: false,
            });
            this.formRef.current?.resetFields();
          }}
          onCreate={this.handleCreate}
        />

      </PageContent>
    )
  }
}

export default SystemComponentPage