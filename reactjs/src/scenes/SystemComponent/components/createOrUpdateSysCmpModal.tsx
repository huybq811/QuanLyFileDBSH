import { Col, Form, Input, Row } from 'antd';
import React from 'react'
import FormModal, { IFormModalProps } from '../../../components/Modal/FormModal';
import SelectItem from '../../../components/Select/SelectItem';
import rules from '../../../constants/index.validation';
import systemService from '../../../services/system/systemService';

export interface ICreateOrUpdateModalProps extends IFormModalProps {
}

function CreateOrUpdateSysCmpModal(props: ICreateOrUpdateModalProps) {

    const renderParentItem = () => {
        return (
            <SelectItem
                formItemProps={{ label: "Chọn hệ thống", name: "systemDataId", rules: rules.required }}
                remoteSource={systemService.getSystemAsOptions}
                refresing={true}
                className='mb-0'
                span={24}
            />
        )
    }

    return (
        <FormModal {...props}>
            <Row gutter={[12, 12]}>
                {renderParentItem()}
                <Col span={24}>
                    <Form.Item label={"Tên hệ thống"} name={'name'} rules={rules.required}>
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label={"Mã hệ thống"} rules={rules.required} name={'code'}>
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label={"Mô tả"} name={'description'}>
                        <Input.TextArea />
                    </Form.Item>
                </Col>
                <Form.Item hidden name={'id'} />
            </Row>
        </FormModal>

    )
}

export default CreateOrUpdateSysCmpModal