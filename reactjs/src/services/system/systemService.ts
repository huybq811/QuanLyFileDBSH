import { EntityDto } from '../dto/entityDto';
import { PagedResultDto } from '../dto/pagedResultDto';
import http from '../httpService';
import { CreateOrUpdateSystemComponentInput, CreateOrUpdateSystemInput, GetAllServiceOutput, PagedSystemResultRequestDto, PagedSystemComponentResultRequestDto, GetAllSystemComponentOutput, PagedFeatureResultRequestDto, GetAllFeatureOutput, CreateOrUpdateFeatureInput, PagedPostResultRequestDto, GetAllPostOutput, CreateOrUpdatePostInput } from './dto/systemDto';

class SystemService {

    public async getAll(pagedFilterAndSortedRequest: PagedSystemResultRequestDto): Promise<PagedResultDto<GetAllServiceOutput>> {
        let result = await http.get('api/services/app/HeThong/GetAll', { params: pagedFilterAndSortedRequest });
        return result.data.result.returnValue;
    }
    public async createOrUpdate(systemInput: CreateOrUpdateSystemInput) {
        let result = await http.post('api/services/app/HeThong/CreateOrUpdate', systemInput);
        return result.data.result;
    }

    public async delete(entityDto: EntityDto) {
        let result = await http.delete('api/services/app/HeThong/Delete', { params: entityDto });
        return result.data;
    }

    public async get(entityDto: EntityDto): Promise<GetAllServiceOutput> {
        let result = await http.get('api/services/app/HeThong/GetById', { params: entityDto });
        return result.data.result;
    }


    public async createOrUpdateSysComponent(systemInput: CreateOrUpdateSystemComponentInput) {
        let result = await http.post('api/services/app/ThanhPhanHeThong/CreateOrUpdate', systemInput);
        return result.data.result;
    }

    public async getAllSysComponent(pagedFilterAndSortedRequest: PagedSystemComponentResultRequestDto): Promise<PagedResultDto<GetAllSystemComponentOutput>> {
        let result = await http.get('api/services/app/ThanhPhanHeThong/GetAll', { params: pagedFilterAndSortedRequest });
        return result.data.result.returnValue;
    }
    public async deleteSysComponent(entityDto: EntityDto) {
        let result = await http.delete('api/services/app/ThanhPhanHeThong', { params: entityDto });
        return result.data;
    }

    public async getSystemAsOptions(): Promise<any> {
        let result = await http.get('api/services/app/HeThong/GetAll',);
        let response = result.data.result?.returnValue?.items;

        const re = response?.map((reservoir: any) => {
            return {
                value: reservoir.id,
                label: reservoir.name
            }
        })
        return re;
    }
    public async getSystemCmpAsOptions(entityDto: any): Promise<any> {
        let result = await http.get('api/services/app/ThanhPhanHeThong/GetListTPHTByHeThong', { params: entityDto });
        let response = result.data.result;
        const re = response?.map((reservoir: any) => {
            return {
                value: reservoir.id,
                label: reservoir.name
            }
        })
        return re;
    }


    public async getAllFeature(pagedFilterAndSortedRequest: PagedFeatureResultRequestDto): Promise<PagedResultDto<GetAllFeatureOutput>> {
        let result = await http.get('api/services/app/ChucNang/GetAll', { params: pagedFilterAndSortedRequest });
        return result.data.result.returnValue;
    }

    public async createOrUpdateFeature(systemInput: CreateOrUpdateFeatureInput) {
        let result = await http.post('api/services/app/ChucNang/CreateOrUpdate', systemInput);
        return result.data.result;
    }

    public async deleteFeature(entityDto: EntityDto) {
        let result = await http.delete('api/services/app/ChucNang/Delete', { params: entityDto });
        return result.data;
    }

    public async getFeatureAsOptions(entityDto: any): Promise<any> {
        let result = await http.get('api/services/app/ChucNang/GetListChucNangByTPHeThong', { params: entityDto });
        let response = result.data.result;
        const re = response?.map((reservoir: any) => {
            return {
                value: reservoir.id,
                label: reservoir.name
            }
        })
        return re;
    }


    public async getAllPost(pagedFilterAndSortedRequest: PagedPostResultRequestDto): Promise<PagedResultDto<GetAllPostOutput>> {
        let result = await http.get('api/services/app/BaiViet/GetAll', { params: pagedFilterAndSortedRequest });
        return result.data.result.returnValue;
    }

    public async createOrUpdatePost(postInput: CreateOrUpdatePostInput) {
        let result = await http.post('api/services/app/BaiViet/CreateOrUpdate', postInput);
        return result.data.result;
    }

    public async deletePost(entityDto: EntityDto) {
        let result = await http.delete('api/services/app/BaiViet/Delete', { params: entityDto });
        return result.data;
    }
}

export default new SystemService();