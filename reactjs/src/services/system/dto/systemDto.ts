import { PagedFilterAndSortedRequest } from '../../dto/pagedFilterAndSortedRequest';

export interface GetAllServiceOutput {
    name: any,
    description: any,
    activated: any,
    isDeleted: any,
    deleterUserId: any,
    deletionTime: any,
    lastModificationTime: any,
    lastModifierUserId: any,
    creationTime: any,
    creatorUserId: any,
    id: any,
}

export interface CreateOrUpdateSystemInput {
    name?: any,
    description?: any,
    id?: any,
}

export interface CreateOrUpdateSystemComponentInput {
    systemDataId: any,
    code: any,
    name: any,
    description: any,
}




export interface PagedSystemResultRequestDto extends PagedFilterAndSortedRequest {
    filter: string
}

export interface PagedSystemComponentResultRequestDto extends PagedFilterAndSortedRequest {
    filter?: string;
    systemDataId?: any;
}

export interface GetAllSystemComponentOutput {
    tenHeThong: any;
    systemDataId: any;
    code: any;
    name: any;
    description: any;
    activated: any;
    isDeleted: any;
    deleterUserId: any;
    deletionTime: any;
    lastModificationTime: any;
    lastModifierUserId: any;
    creationTime: any;
    creatorUserId: any;
    id: any;
}

export interface PagedFeatureResultRequestDto extends PagedFilterAndSortedRequest {
    filter: string;
    systemDataId: any;
    objectSystemId: any
}

export interface CreateOrUpdateFeatureInput {
    objectSystemId: any;
    systemDataId: any;
    name: any;
    description: any;
}

export interface GetAllFeatureOutput {
    systemDataName: any;
    objectSystemName: any;
    objectSystemId: any;
    systemDataId: any;
    name: any;
    description: any;
    activated: any;
    isDeleted: any;
    deleterUserId: any;
    deletionTime: any;
    lastModificationTime: any;
    lastModifierUserId: any;
    creationTime: any;
    creatorUserId: any;
    id: any;
}

export interface PagedPostResultRequestDto extends PagedFilterAndSortedRequest {
    filter: string;
    systemDataId: any;
    objectSystemId: any;
    objectCategoryId: any;
}

export interface GetAllPostOutput {
    fileUploadtOutputDtos: any;
    systemDataId: any;
    objectSystemId: any;
    objectCategoryId: any;
    title: any;
    content: any;
    description: any;
    activated: any;
    isDeleted: any;
    deleterUserId: any;
    deletionTime: any;
    lastModificationTime: any;
    lastModifierUserId: any;
    creationTime: any;
    creatorUserId: any;
    id: any;
}
export interface CreateOrUpdatePostInput {
    id: any;
    creationTime: any;
    creatorUserId: any;
    lastModificationTime: any;
    lastModifierUserId: any;
    isDeleted: any;
    deleterUserId: any;
    deletionTime: any;
    systemDataId: any;
    objectSystemId: any;
    objectCategoryId: any;
    title: any;
    content: any;
    description: any;
    activated: any;
    fileUploadtInputDtos: any;
}