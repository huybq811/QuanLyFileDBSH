import { action, observable } from 'mobx';
import { EntityDto } from '../services/dto/entityDto';
import { PagedResultDto } from "../services/dto/pagedResultDto";
import { GetAllServiceOutput, CreateOrUpdateSystemInput, PagedSystemResultRequestDto, GetAllSystemComponentOutput, PagedSystemComponentResultRequestDto, CreateOrUpdateSystemComponentInput, GetAllFeatureOutput, CreateOrUpdateFeatureInput, PagedFeatureResultRequestDto, GetAllPostOutput, PagedPostResultRequestDto, CreateOrUpdatePostInput } from '../services/system/dto/systemDto'
import systemService from '../services/system/systemService';

class SystemStore {
    @observable systemList!: PagedResultDto<GetAllServiceOutput>;
    @observable systemComponentList!: PagedResultDto<GetAllSystemComponentOutput>;
    @observable featureList!: PagedResultDto<GetAllFeatureOutput>;
    @observable postList!: PagedResultDto<GetAllPostOutput>;

    @action
    async createOreUpdate(createUserInput: CreateOrUpdateSystemInput) {
        await systemService.createOrUpdate(createUserInput);
        //   this.users.items.push(result);
    }
    @action
    async delete(entityDto: EntityDto) {
        await systemService.delete(entityDto);
    }
    @action
    async getAll(pagedFilterAndSortedRequest: PagedSystemResultRequestDto) {
        let result = await systemService.getAll(pagedFilterAndSortedRequest);
        this.systemList = result;
    }

    @action
    async createOreUpdateSysCmp(createUserInput: CreateOrUpdateSystemComponentInput) {
        await systemService.createOrUpdateSysComponent(createUserInput);
    }
    @action
    async deleteSysCmp(entityDto: EntityDto) {
        await systemService.deleteSysComponent(entityDto);
    }
    @action
    async getAllSysCmp(pagedFilterAndSortedRequest: PagedSystemComponentResultRequestDto) {
        let result = await systemService.getAllSysComponent(pagedFilterAndSortedRequest);
        this.systemComponentList = result;
    }

    @action
    async createOreUpdateFeature(createUserInput: CreateOrUpdateFeatureInput) {
        await systemService.createOrUpdateFeature(createUserInput);
    }
    @action
    async deleteFeature(entityDto: EntityDto) {
        await systemService.deleteFeature(entityDto);
    }
    @action
    async getAllFeature(pagedFilterAndSortedRequest: PagedFeatureResultRequestDto) {
        let result = await systemService.getAllFeature(pagedFilterAndSortedRequest);
        this.featureList = result;
    }


    @action
    async createOreUpdatePost(createUserInput: CreateOrUpdatePostInput) {
        await systemService.createOrUpdatePost(createUserInput);
    }
    @action
    async deletePost(entityDto: EntityDto) {
        await systemService.deletePost(entityDto);
    }
    @action
    async getAllPost(pagedFilterAndSortedRequest: PagedPostResultRequestDto) {
        let result = await systemService.getAllPost(pagedFilterAndSortedRequest);
        this.postList = result;
    }


}

export default SystemStore;