﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RIS.Migrations
{
    /// <inheritdoc />
    public partial class update_table_ObjectFile_19032024 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDownload",
                table: "ObjectFile",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDownload",
                table: "ObjectFile");
        }
    }
}
