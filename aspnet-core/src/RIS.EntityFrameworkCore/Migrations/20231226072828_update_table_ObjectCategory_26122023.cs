﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RIS.Migrations
{
    /// <inheritdoc />
    public partial class update_table_ObjectCategory_26122023 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SystemDataId",
                table: "ObjectCategory",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SystemDataId",
                table: "ObjectCategory");
        }
    }
}
