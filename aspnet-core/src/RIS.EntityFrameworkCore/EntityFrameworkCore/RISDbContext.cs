﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using RIS.Authorization.Roles;
using RIS.Authorization.Users;
using RIS.MultiTenancy;
using RIS.EntitiesDb;

namespace RIS.EntityFrameworkCore
{
    public class RISDbContext : AbpZeroDbContext<Tenant, Role, User, RISDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<ObjectDetail> ObjectDetail { get; set; }
        public virtual DbSet<ObjectFile> ObjectFile { get; set; }

        public RISDbContext(DbContextOptions<RISDbContext> options)
            : base(options)
        {
        }
    }
}
