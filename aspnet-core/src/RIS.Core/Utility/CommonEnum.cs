﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIS.Utility
{
    public static class CommonEnum
    {
        public enum TypeFile
        {
            Document = 1,
            Map = 2,
            Picture = 3
        }
    }
}
