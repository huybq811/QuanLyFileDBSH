﻿using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;
using RIS.Dto;
using RIS.EntitiesDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RIS.Dto
{
    [AutoMap(typeof(ObjectFile))]
    public class FileUploadInputDto : ObjectFile
    {
        public byte[] FileUpload { get; set; }
        //public IFormFile FileUpload { get; set; }
    }

    public class FileUploadDto : PagedAndFilteredInputDto
    {
    }
    public class FileUploadOutputDto : ObjectFile
    {
        public string ViewUrl { get; set; }
    }
}
