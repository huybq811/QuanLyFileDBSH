﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ris.ApplicationDto;
using RIS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIS.RisAppCore
{
    public interface IBaiVietAppService : IApplicationService
    {
        Task<CommonResponseDto> GetAll(BaiVietDto input);
        Task<CommonResponseDto> CreateOrUpdate(BaiVietInputDto input);
        Task<CommonResponseDto> GetById(string objectDetailId);
        Task<CommonResponseDto> Delete(string objectDetailId);
        FileStreamResult DownloadFile(long fileId);
        Task<CommonResponseDto> WriteFileFormFile(IFormFile fileUpload, string objectDetailId, string name, bool isdownload = false);
        Task<CommonResponseDto> DeleteFile(long fileId);
    }
}
